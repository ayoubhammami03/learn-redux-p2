import counterReducer from "./counter";
import LoggedReducer from "./isLogged";
import { combineReducers } from "redux";

const allReducer = combineReducers({
  counter: counterReducer,
  isLogged: LoggedReducer,
});
export default allReducer;
